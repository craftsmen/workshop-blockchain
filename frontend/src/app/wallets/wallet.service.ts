import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { MessageService } from '../messages/message.service';

import { Wallet } from '../api';

import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';

@Injectable()
export class WalletService {
  lastQuery = '';

  constructor(private http: HttpClient, private messageService: MessageService) {}

  findWallet(name: string): Observable<Wallet> {
    this.messageService.log('Fetching wallet...');

    return this.http.get<Wallet>(`/api/wallet/${name}`)
      .do(() => this.messageService.log(`Wallet found.`))
      .catch(() => {
        this.messageService.log(`Wallet NOT found!`);

        return Observable.empty<Wallet>();
      });
  }
}
