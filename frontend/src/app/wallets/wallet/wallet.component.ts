import { Component, Input, OnChanges, OnDestroy } from '@angular/core';
import { transition, trigger, useAnimation } from '@angular/animations';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/withLatestFrom';

import { WalletService } from '../wallet.service';
import { Transaction, Wallet } from '../../api';
import { slideAnimation } from '../../slide.animation';

@Component({
  selector: 'app-wallet',
  templateUrl: './wallet.component.html',
  styleUrls: ['./wallet.component.scss'],
  animations: [
    trigger('slide', [
      transition('* => *', [
        useAnimation(slideAnimation),
      ]),
    ]),
  ],
})
export class WalletComponent implements OnChanges, OnDestroy {
  @Input() name: string;

  wallet: Wallet;

  private subscription: Subscription;

  constructor(private walletService: WalletService) {}

  ngOnChanges(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }

    this.subscription = Observable.timer(0, 5000)
      .switchMap(() => this.walletService.findWallet(this.name))
      .subscribe(wallet => this.wallet = wallet);
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  trackByFn(index: number, transaction: Transaction): string {
    return transaction.id;
  }
}
