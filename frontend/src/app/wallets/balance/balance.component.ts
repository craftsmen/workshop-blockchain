import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ReplaySubject } from 'rxjs/ReplaySubject';

import 'rxjs/add/observable/of';
import 'rxjs/add/observable/timer';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/concat';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/take';

@Component({
  selector: 'app-balance',
  templateUrl: './balance.component.html'
})
export class BalanceComponent implements OnInit, OnChanges {
  @Input() balance: number;

  currentBalance = 0;

  private nextBalance = new ReplaySubject<number>();

  ngOnInit(): void {
    this.nextBalance
      .switchMap(next =>
        Observable.timer(0, 30)
          .take(20)
          .map(i => this.currentBalance + i * (next - this.currentBalance) / 20)
          .concat(Observable.of(next))
      )
      .subscribe(b => this.currentBalance = b);
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.nextBalance.next(this.balance);
  }
}
