import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { CreateTransactionComponent } from './transactions/create/create-transaction.component';
import { TransactionService } from './transactions/transaction.service';
import { MessagesComponent } from './messages/messages.component';
import { MessageService } from './messages/message.service';
import { BlockchainComponent } from './blockchain/blockchain.component';
import { PendingTransactionsComponent } from './transactions/pending/pending-transactions.component';
import { BlockchainService } from './blockchain/blockchain.service';
import { PeersComponent } from './peers/peers.component';
import { PeersService } from './peers/peers.service';
import { WalletComponent } from './wallets/wallet/wallet.component';
import { WalletsComponent } from './wallets/wallets.component';
import { WalletService } from './wallets/wallet.service';
import { BalanceComponent } from './wallets/balance/balance.component';

const routes: Routes = [
  { path: '', redirectTo: '/transactions', pathMatch: 'full' },
  { path: 'transactions', component: PendingTransactionsComponent },
  { path: 'transactions/create', component: CreateTransactionComponent },
  { path: 'blockchain', component: BlockchainComponent },
  { path: 'peers', component: PeersComponent },
  { path: 'wallets', component: WalletsComponent },
];

@NgModule({
  declarations: [
    AppComponent,
    PendingTransactionsComponent,
    CreateTransactionComponent,
    CreateTransactionComponent,
    MessagesComponent,
    BlockchainComponent,
    PeersComponent,
    WalletComponent,
    WalletsComponent,
    BalanceComponent
  ],
  imports: [
    BrowserAnimationsModule,
    FormsModule,
    RouterModule.forRoot(routes, { useHash: true }),
    HttpClientModule
  ],
  providers: [TransactionService, MessageService, BlockchainService, PeersService, WalletService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
