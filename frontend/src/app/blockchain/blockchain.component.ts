import { Component, OnDestroy, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { transition, trigger, useAnimation } from '@angular/animations';

import 'rxjs/add/observable/timer';
import 'rxjs/add/operator/switchMap';

import { Block, Blockchain } from '../api';

import { BlockchainService } from './blockchain.service';
import { slideAnimation } from '../slide.animation';

@Component({
  selector: 'app-blockchain',
  templateUrl: './blockchain.component.html',
  styleUrls: ['./blockchain.component.scss'],
  animations: [
    trigger('slide', [
      transition('* => *', [
        useAnimation(slideAnimation),
      ]),
    ]),
  ],
})
export class BlockchainComponent implements OnInit, OnDestroy {
  blockchain: Blockchain;

  private subscription: Subscription;

  constructor(private blockchainService: BlockchainService) {}

  ngOnInit(): void {
    this.subscription = Observable.timer(0, 5000)
      .switchMap(() => this.blockchainService.getBlockchain())
      .subscribe(blockchain => this.blockchain = blockchain);
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  trackByFn(index: number, block: Block): number {
    return block.index;
  }

  mine(): void {
    this.blockchainService.doMine().subscribe(block => {
      if (block) {
        this.blockchain.chain.unshift(block);
      }
    });
  }
}
