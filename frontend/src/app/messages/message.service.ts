import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ReplaySubject } from 'rxjs/ReplaySubject';

export interface Message {
  text: string;
  time: Date;
}

@Injectable()
export class MessageService {
  messages$: Observable<Message[]>;

  private messages: Message[] = [];
  private messagesSubject = new ReplaySubject<Message[]>();

  constructor() {
    this.messages$ = this.messagesSubject.asObservable();
  }

  log(message: string): void {
    this.messages.push({
      text: message,
      time: new Date(),
    });

    this.messagesSubject.next(this.messages);
  }

  clear(): void {
    this.messages = [];
    this.messagesSubject.next(this.messages);
  }
}
