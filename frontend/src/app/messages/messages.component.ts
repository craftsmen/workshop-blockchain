import { Component, OnInit } from '@angular/core';
import { transition, trigger, useAnimation } from '@angular/animations';

import { slideAnimation } from '../slide.animation';

import { Message, MessageService } from './message.service';

const MESSAGES_LENGTH = 10;

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  animations: [
    trigger('slide', [
      transition('* => *', [
        useAnimation(slideAnimation),
      ]),
    ]),
  ],
})
export class MessagesComponent implements OnInit {
  messages: Message[] = [];

  constructor(private messageService: MessageService) {}

  ngOnInit(): void {
    this.messageService.messages$.subscribe(messages => this.messages = messages.slice(-1 * MESSAGES_LENGTH));
  }

  trackByFn(index: number, message: Message): number {
    return message.time.getTime();
  }

  clear(): void {
    this.messageService.clear();
  }
}
