package nl.craftsmen.blockchain.craftscoinnode.transaction;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;


public class TransactionTest {

    private ObjectMapper objectMapper = new ObjectMapper();

    @Test
    public void testIfTransactionHasCorrectFormat() throws JsonProcessingException {
        Transaction transaction = new Transaction();
        String transactionAsJson = objectMapper.writeValueAsString(transaction);
        assertThat(transactionAsJson).isEqualTo("{\"amount\":null,\"from\":null,\"id\":null,\"publicKey\":null,\"signature\":null,\"to\":null}");
    }

    @Test
    public void testIfEqualsTestsOnSameIdOnly() throws IOException {
        String transactionAsJson = "{\"id\":\"4c9e82bd-ef03-4b30-8b70-95c63aa5b188\", \"amount\":3,\"from\":\"michel\",\"to\":\"gerry\"}";
        String transactionAsJson2 = "{\"id\":\"4c9e82bd-ef03-4b30-8b70-95c63aa5b188\", \"amount\":4,\"from\":\"gerry\",\"to\":\"michel\"}";
        Transaction transaction = objectMapper.readValue(transactionAsJson, Transaction.class);
        Transaction transaction2 = objectMapper.readValue(transactionAsJson2, Transaction.class);
        assertThat(transaction).isEqualTo(transaction2);
        assertThat(transaction.hashCode()).isEqualTo(transaction2.hashCode());
    }

    @Test
    public void testIfDifferentIdsAreRegardedAsTwoDifferentTransactions() throws IOException {
        String transactionAsJson = "{\"id\":\"4c9e82bd-ef03-4b30-8b70-95c63aa5b189\", \"amount\":3,\"from\":\"michel\",\"to\":\"gerry\"}";
        String transactionAsJson2 = "{\"id\":\"4c9e82bd-ef03-4b30-8b70-95c63aa5b188\", \"amount\":4,\"from\":\"gerry\",\"to\":\"michel\"}";
        Transaction transaction = objectMapper.readValue(transactionAsJson, Transaction.class);
        Transaction transaction2 = objectMapper.readValue(transactionAsJson2, Transaction.class);
        assertThat(transaction).isNotEqualTo(transaction2);
        assertThat(transaction.hashCode()).isNotEqualTo(transaction2.hashCode());
    }

}