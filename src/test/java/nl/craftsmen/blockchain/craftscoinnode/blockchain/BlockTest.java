package nl.craftsmen.blockchain.craftscoinnode.blockchain;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class BlockTest {

    private ObjectMapper objectMapper = new ObjectMapper();

    @Test
    public void blockHasCorrectFormat() throws JsonProcessingException {
        Block block = new Block();
        String blockAsJson = objectMapper.writeValueAsString(block);
        assertThat(blockAsJson).isEqualTo("{\"index\":0,\"previousHash\":null,\"proof\":0,\"timestamp\":0,\"transactions\":null}");
    }

}