package nl.craftsmen.blockchain.craftscoinnode.network;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashSet;
import java.util.Set;

@JsonPropertyOrder(alphabetic = true)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, getterVisibility = JsonAutoDetect.Visibility.NONE, isGetterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE)
class Peers {

    private Set<String> peers = new HashSet<>();

    Set<String> getPeers() {
        return peers;
    }

    void setPeers(Set<String> peers) {
        this.peers = peers;
    }

    boolean isEmpty() {
        return peers.isEmpty();
    }

    @Override
    public String toString() {
        return "Peers{" +
                "peers=" + peers +
                '}';
    }
}
