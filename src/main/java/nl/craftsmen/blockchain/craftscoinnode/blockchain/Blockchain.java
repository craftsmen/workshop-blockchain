package nl.craftsmen.blockchain.craftscoinnode.blockchain;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import nl.craftsmen.blockchain.craftscoinnode.transaction.Transaction;
import org.apache.commons.codec.digest.DigestUtils;

import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Implementation of the blockchain.
 */
@JsonPropertyOrder(alphabetic = true)
@JsonAutoDetect(fieldVisibility = Visibility.ANY, getterVisibility = Visibility.NONE, isGetterVisibility = Visibility.NONE, setterVisibility = Visibility.NONE)
public class Blockchain {

    //TODO add chain field

    /**
     * Factory method to create a new blockchain.
     *
     * @return a new blockchain containing a genesis block.
     */
    static Blockchain create() {
        //TODO implement me
        return null;
    }

    //default constructor, needed for json deserialization
    @SuppressWarnings({"unused", "WeakerAccess"})
    public Blockchain() {
    }

    /**
     * Mines a new block. Adds all the transactions,
     * calculates the proof and puts a createHashOf of the previous block in the new block.
     *
     * @param transactionsToBeIncluded the transactions the be included in the block.
     * @return the new block.
     */
    Block mineNewBlock(Set<Transaction> transactionsToBeIncluded, String miningWalletId) {
        //TODO implement me
        return null;
    }

    /**
     * Adds block to the blockchain.
     *
     * @param block the block to be added.
     */
    void addBlock(Block block) {
        //TODO implement me
    }


    /**
     * Gets the index of the block height of the next block.
     *
     * @return index of the block height of the next block.
     */
    long getIndexOfNextBlock() {
        //TODO implement me
        return 0;
    }

    /**
     * Gets all transactions in the blockchain.
     *
     * @return all transactions in the blockchain.
     */
    List<Transaction> getAllTransactions() {
        //TODO implement me
        return null;
    }

    /**
     * Gets transactions in the blockchain for a specific wallet.
     *
     * @param walletId the walletId.
     * @return transactions in the blockchain for a specific wallet.
     */
    public List<Transaction> getTransactionsFor(String walletId) {
        //TODO implement me
        return null;
    }

    /**
     * Checks if this blockchain is valid.
     * A blockchain is valid if all the proofs are correct and all previous hashes point to the createHashOf of the last block.
     *
     * @return if this blockchain is valid.
     */
    boolean isValid() {
        //TODO implement me
        return false;
    }

    /**
     * Checks if a new block is valid with respect to this blockchain.
     * A new block is valid if the proof of the new block is correct and the previoushash matches the createHashOf of the block before.
     *
     * @param newBlock the new block.
     * @return true if the new block is valid, false otherwise.
     */
    boolean isNewBlockValid(Block newBlock) {
        //TODO implement me
        return false;
     }

    boolean isInferiorTo(Blockchain otherChain) {
        //TODO implement me
        return false;
    }

    private String createHashOf(Block block) {
        return DigestUtils.sha256Hex(block.toString());
    }

    //TODO toString
}
