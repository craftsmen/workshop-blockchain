package nl.craftsmen.blockchain.craftscoinnode.blockchain;

import nl.craftsmen.blockchain.craftscoinnode.CraftsCoinConfigurationProperties;
import nl.craftsmen.blockchain.craftscoinnode.network.Network;
import nl.craftsmen.blockchain.craftscoinnode.transaction.SignatureService;
import nl.craftsmen.blockchain.craftscoinnode.transaction.Transaction;
import nl.craftsmen.blockchain.craftscoinnode.transaction.TransactionPool;
import nl.craftsmen.blockchain.craftscoinnode.util.GenericRepository;
import nl.craftsmen.blockchain.craftscoinnode.util.InstanceInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.*;

/**
 * Manages the blockchain on this node.
 */
@Component
public class BlockchainService {

    private static final Logger LOGGER = LoggerFactory.getLogger(BlockchainService.class);

    private final Network network;
    private final SignatureService signatureService;
    private final GenericRepository genericRepository;
    private final TransactionPool transactionPool;
    private final InstanceInfo instanceInfo;
    private final String miningWalletId;

    @Autowired
    public BlockchainService(Network network, SignatureService signatureService, GenericRepository genericRepository, TransactionPool transactionPool, InstanceInfo instanceInfo, CraftsCoinConfigurationProperties configuration) {
        this.network = network;
        this.signatureService = signatureService;
        this.genericRepository = genericRepository;
        this.transactionPool = transactionPool;
        this.instanceInfo = instanceInfo;
        this.miningWalletId = configuration.getMiningWalletId();
    }


    /**
     * Initializes and updates the blockchain on startup.
     */
    public void init() {
        LOGGER.info("intializing");
        initializeBlockchain();
        reachConsensus();
    }

    private void initializeBlockchain() {
        //TODO implement me
    }

    /**
     * Retrieves the blockchain of this node for API usage.
     *
     * @return the blockchain of this node.
     */
    public Blockchain retrieveBlockChain() {
        //TODO implement me
        return null;
    }

    /**
     * Creates a new transaction and adds it to the transaction pool.
     *
     * @param from   where the money comes from
     * @param to     where the money should be sent
     * @param amount how much
     * @return the block height of the block that this transaction will be mined in.
     */
    public long createTransaction(String from, String to, BigDecimal amount) {
        //TODO implement me
        return 0;
    }

    /**
     * Gets pending transactions for API usage.
     *
     * @return the pending transactions from the transaction pool.
     */
    public Set<Transaction> getPendingTransactions() {
        //TODO implement me
        return null;
    }

    /**
     * Mines a new block with all transactions in the transaction pool.
     *
     * @return the new block that has just been mined.
     */
    public Block mine() {
        //TODO implement me
        return null;
    }

    /**
     * Establishes consensus over the current state of the blockchain.
     * If another node has a blockchain that is 'better', the blockchain of the current node will be replaced.
     */
    private void reachConsensus() {
        //TODO implement me
    }

    /**
     * Adds a newly received transaction that has not yet been received
     * to the transaction pool and propagates it further into the network.
     *
     * @param transaction the new transaction.
     * @param sourcePeer  the peer that sent the notification.
     */
    public void newTransactionReceived(Transaction transaction, String sourcePeer) {
            //TODO implement me

        }

    /**
     * Adds a newly received block (if valid) to the blockchain and propagates it further into the network.
     *
     * @param block      the new block.
     * @param sourcePeer the peer that sent the notification.
     */
    public void newBlockReceived(Block block, String sourcePeer) {
        //TODO implement me
    }


    private void clearConfirmedTransactions() {
        //TODO implement me
    }

    /**
     * Returns if the blockchain on this node is valid.
     *
     * @return if the blockchain on this node is valid.
     */
    public boolean isValid() {
        //TODO implement me
        return false;
    }

}
