package nl.craftsmen.blockchain.craftscoinnode.transaction;

import nl.craftsmen.blockchain.craftscoinnode.util.GenericRepository;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.security.*;
import java.security.spec.ECGenParameterSpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.Optional;

@Component
public class SignatureService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SignatureService.class);
    private static final String ALGORITHM = "ECDSA";
    private static final String BC = "BC";

    private final GenericRepository genericRepository;
    private KeyPair keyPair;

    @Autowired
    public SignatureService(GenericRepository genericRepository) {
        this.genericRepository = genericRepository;
    }

    public void init() {
        try {
            Security.addProvider(new BouncyCastleProvider());
            Optional<Keys> keys = genericRepository.load(Keys.class);
            if (keys.isPresent()) {
                LOGGER.info("existing key material found, loading...");
                loadExistingKeyData(keys.get());
                LOGGER.info("finished loading key material.");
            } else {
                LOGGER.info("no key material found, generating new keypair.");
                generateNewKeyPair();
                Keys serializedKeys = new Keys(new String(Base64.getEncoder().encode(keyPair.getPrivate().getEncoded())), new String(Base64.getEncoder().encode(keyPair.getPublic().getEncoded())));
                genericRepository.save(serializedKeys);
                LOGGER.info("new keypair generated and saved.");
            }
        } catch (GeneralSecurityException e) {
            throw new RuntimeException(e);
        }
    }

    private void loadExistingKeyData(Keys keys) throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeySpecException {
        KeyFactory kf = KeyFactory.getInstance(ALGORITHM, BC);
        byte[] bytePrivateKey = Base64.getDecoder().decode(keys.getPrivateKey().getBytes());
        PKCS8EncodedKeySpec pkcs8privateKey = new PKCS8EncodedKeySpec(bytePrivateKey);
        PrivateKey privateKey = kf.generatePrivate(pkcs8privateKey);

        byte[] bytePublicKey = Base64.getDecoder().decode(keys.getPublicKey().getBytes());
        X509EncodedKeySpec X509publicKey = new X509EncodedKeySpec(bytePublicKey);
        PublicKey publicKey = kf.generatePublic(X509publicKey);
        this.keyPair = new KeyPair(publicKey, privateKey);
    }

    private void generateNewKeyPair() throws NoSuchAlgorithmException, NoSuchProviderException, InvalidAlgorithmParameterException {
        KeyPairGenerator keyGen = KeyPairGenerator.getInstance(ALGORITHM, BC);
        SecureRandom random = SecureRandom.getInstance("SHA1PRNG");
        ECGenParameterSpec ecSpec = new ECGenParameterSpec("prime192v1");
        keyGen.initialize(ecSpec, random);
        this.keyPair = keyGen.generateKeyPair();
    }


    public String sign(String payload) {
        try {
            LOGGER.info("signing payload...");
            Signature dsa = Signature.getInstance(ALGORITHM, BC);
            dsa.initSign(keyPair.getPrivate());
            dsa.update(payload.getBytes());
            return Base64.getEncoder().encodeToString(dsa.sign());
        } catch (GeneralSecurityException e) {
            throw new RuntimeException(e);
        }
    }

    public boolean verify(String payload, String signature, String publicKey) {
        try {
            LOGGER.info("verifying signature...");
            Signature dsa = Signature.getInstance(ALGORITHM, BC);
            byte[] bytePublicKey = Base64.getDecoder().decode(publicKey.getBytes());
            KeyFactory kf = KeyFactory.getInstance(ALGORITHM, BC);
            X509EncodedKeySpec X509publicKey = new X509EncodedKeySpec(bytePublicKey);
            PublicKey incomingPublicKey = kf.generatePublic(X509publicKey);
            dsa.initVerify(incomingPublicKey);
            dsa.update(payload.getBytes());
            return dsa.verify(Base64.getDecoder().decode(signature.getBytes()));
        } catch (GeneralSecurityException e) {
            throw new RuntimeException(e);
        }
    }

    public String getPublicKey() {
        return new String(Base64.getEncoder().encode(keyPair.getPublic().getEncoded()));
    }


}
