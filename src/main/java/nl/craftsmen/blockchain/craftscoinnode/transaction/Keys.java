package nl.craftsmen.blockchain.craftscoinnode.transaction;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder(alphabetic = true)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, getterVisibility = JsonAutoDetect.Visibility.NONE, isGetterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE)
class Keys {

    private String privateKey;
    private String publicKey;

    //default public constructor, needed for json deserialization
    @SuppressWarnings({"unused", "WeakerAccess"})
    public Keys() {
    }
    Keys(String privateKey, String publicKey) {
        this.privateKey = privateKey;
        this.publicKey = publicKey;
    }

    String getPrivateKey() {
        return privateKey;
    }

    String getPublicKey() {
        return publicKey;
    }
}
