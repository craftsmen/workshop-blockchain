package nl.craftsmen.blockchain.craftscoinnode.wallet;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import nl.craftsmen.blockchain.craftscoinnode.transaction.Transaction;

import java.math.BigDecimal;
import java.util.List;


@JsonPropertyOrder(alphabetic = true)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, getterVisibility = JsonAutoDetect.Visibility.NONE, isGetterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE)
public class Wallet {

    private BigDecimal balance;
    private List<Transaction> minedTransactions;
    private List<Transaction> unconfirmedTransactions;

    @SuppressWarnings({"unused", "WeakerAccess"})
    public Wallet() {
    }

    Wallet(BigDecimal balance, List<Transaction> minedTransactions, List<Transaction> unconfirmedTransactions) {
        this.balance = balance;
        this.minedTransactions = minedTransactions;
        this.unconfirmedTransactions = unconfirmedTransactions;
    }

    @SuppressWarnings("unused")
    public BigDecimal getBalance() {
        return balance;
    }

    @SuppressWarnings("unused")
    public List<Transaction> getMinedTransactions() {
        return minedTransactions;
    }

    @SuppressWarnings("unused")
    public List<Transaction> getUnconfirmedTransactions() {
        return unconfirmedTransactions;
    }
}
